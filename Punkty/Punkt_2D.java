public class Punkt_2D extends Punkt_1D{
    protected double y;

    Punkt_2D(double x, double y){
        this.x=x;
        this.y=y;
    }
    Punkt_2D(){
        x=0;
        y=0;
    }
    public void setY(double y){
        this.y=y;
    }
    public double getY(){
        return y;
    }
    public double ile(){
        return Math.sqrt((x*x) + (y*y));
    }
    public String toString() {
        return "("+Double.toString(x)+","+Double.toString(y)+")";
    }
    public int compareTo(Punkt_2D o) {
        String b = String.valueOf(o.ile());
        int a = String.valueOf(ile()).compareTo(b);
        return a;
    }

}
