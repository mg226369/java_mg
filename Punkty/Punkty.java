import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.*;

public class Punkty {

    public static void main(String agrs[]) throws FileNotFoundException {
        File plik = new File("plik.txt");
        Scanner odczyt = new Scanner(plik);
        List<Punkt_1D> lista = new ArrayList<>();
        while(odczyt.hasNextLine()){
            String linia = odczyt.nextLine();
            String tabela[]= linia.split(",");
            if (tabela.length == 3){
                Punkt_3D pkt = new Punkt_3D(Double.parseDouble(tabela[0]),Double.parseDouble(tabela[1]),Double.parseDouble(tabela[2]));
                lista.add(pkt);
            }
            if (tabela.length == 2){
                Punkt_2D pkt = new Punkt_2D(Double.parseDouble(tabela[0]),Double.parseDouble(tabela[1]));
                lista.add(pkt);
            }
            if(tabela.length == 1){
                Punkt_1D pkt = new Punkt_1D(Double.parseDouble(tabela[0]));
                lista.add(pkt);
            }
        }
        System.out.println("punkty w pliku: ");
        for(int i =0;i<lista.size();i++) System.out.println(lista.get(i)+" odleglosc "+ lista.get(i).ile());
        Collections.sort(lista);
        System.out.println("#################### Po Sortowaniu: ####### ");
        for(int i =0;i<lista.size();i++) System.out.println(lista.get(i)+" odleglosc "+ lista.get(i).ile());

    }
}