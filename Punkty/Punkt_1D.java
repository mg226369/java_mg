public class Punkt_1D implements Comparable<Punkt_1D>{

    protected double x;

    Punkt_1D(double x){
        this.x=x;
    }
    Punkt_1D(){
        x=0;
    }

    public void setX(double x){
        this.x=x;
    }
    public double getX(){
        return x;
    }
    public double ile(){
            return x;
    }

    @Override
    public String toString() {
        return "("+Double.toString(x)+")";
    }
    public int compareTo(Punkt_1D o) {
        String b = String.valueOf(o.ile());
        int a = String.valueOf(ile()).compareTo(b);
            return a;
        }


}
