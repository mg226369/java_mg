package com.company;

public class Punkt {
    int x;
    int y;
    Punkt(){
        x = 0;
        y = 0;
    }
    Punkt(int x,int y){
        this.x=x;
        this.y=y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}