package com.company;
import java.util.LinkedList;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Grafika extends JPanel implements KeyListener, MouseListener {
    protected LinkedList<Figura> lista ;
    protected int zmienna = 0;

    public Grafika(LinkedList<Figura> figurki) {
        this.lista = figurki;
        setFocusable(true);
        addKeyListener(this);
        addMouseListener(this);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setStroke(new BasicStroke(3.0f));

        for(int i =0;i<lista.size();i++){
            if (lista.get(i).getClass()== Okrag.class){
                Okrag o = (Okrag) lista.get(i);
                g2d.setColor(Color.GREEN);
                g2d.draw(o.getShape());
            }
            if (lista.get(i).getClass()== Kwadrat.class){
                Kwadrat k = (Kwadrat) lista.get(i);
                g2d.setColor(Color.RED);
                g2d.draw(k.getShape());
            }
            if (lista.get(i).getClass()== Trojkat.class){
                Trojkat t = (Trojkat) lista.get(i);
                g2d.setColor(Color.YELLOW);
                g2d.draw(t.getShape());
            }
        }
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        ///                                                        Usuwanie zaznaczonej figury
        if(e.getKeyCode() == KeyEvent.VK_DELETE) {
            System.out.println("nacisnales delete: "+ lista.size());
            lista.remove(zmienna);
            System.out.println("po usunieciu"+ lista.size());
            this.repaint();
        }
        ////                                                        Przsuwanie w góre
        if (e.getKeyCode() == KeyEvent.VK_UP){
            System.out.println("strzalka w gore");
            ////////////// OKRAG
            if(lista.get(zmienna).getClass() == Okrag.class){
                Okrag o = (Okrag) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
               punkt.y = o.getP().y-punkt.y;
               punkt.x=o.getP().x;
               o.setP(punkt);
               this.repaint();
            }
            /////////////// KWADRAT
            if(lista.get(zmienna).getClass() == Kwadrat.class){
                Kwadrat k = (Kwadrat) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
                punkt.y = k.getP().y-punkt.y;
                punkt.x=k.getP().x;
                k.setP(punkt);
                this.repaint();
            }
            ////////////////// TROJKAT
            if(lista.get(zmienna).getClass() == Trojkat.class){
                Trojkat t = (Trojkat) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
                Punkt punkt1 = new Punkt(0,20);
                Punkt punkt2 = new Punkt(0,20);
                //Ustawnienie punktu A
                punkt.y = t.getA().y-punkt.y;
                punkt.x=t.getA().x;
                t.setA(punkt);
                /////Ustawnienie punktu B
                punkt1.y = t.getB().y-punkt1.y;
                punkt1.x=t.getB().x;
                t.setB(punkt1);
                /////Ustawnienie punktu C
                punkt2.y = t.getC().y-punkt2.y;
                punkt2.x=t.getC().x;
                t.setC(punkt2);
                this.repaint();
            }
        }
        ////                                                            Przsuwanie w dół
        if (e.getKeyCode() == KeyEvent.VK_DOWN){
            System.out.println("strzalka w dol");
            if(lista.get(zmienna).getClass() == Okrag.class){
                Okrag o = (Okrag) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
                punkt.y = o.getP().y+punkt.y;
                punkt.x=o.getP().x;
                o.setP(punkt);
                this.repaint();
            }
            if(lista.get(zmienna).getClass() == Kwadrat.class){
                Kwadrat k = (Kwadrat) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
                punkt.y = k.getP().y+punkt.y;
                punkt.x=k.getP().x;
                k.setP(punkt);
                this.repaint();
            }
            if(lista.get(zmienna).getClass() == Trojkat.class){
                Trojkat t = (Trojkat) lista.get(zmienna);
                Punkt punkt = new Punkt(0,20);
                Punkt punkt1 = new Punkt(0,20);
                Punkt punkt2 = new Punkt(0,20);
                //Ustawnienie punktu A
                punkt.y = t.getA().y+punkt.y;
                punkt.x=t.getA().x;
                t.setA(punkt);
                /////Ustawnienie punktu B
                punkt1.y = t.getB().y+punkt1.y;
                punkt1.x=t.getB().x;
                t.setB(punkt1);
                /////Ustawnienie punktu C
                punkt2.y = t.getC().y+punkt2.y;
                punkt2.x=t.getC().x;
                t.setC(punkt2);
                this.repaint();
            }

        }
        //////////////                                              PRZESUWANIE W LEWO
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            System.out.println("strzalka w lewo");
            ////////////////// OKRAG
            if (lista.get(zmienna).getClass() == Okrag.class) {
                Okrag o = (Okrag) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                punkt.x = o.getP().x - punkt.x;
                punkt.y = o.getP().y;
                o.setP(punkt);
                this.repaint();
            }
            ////////////////// KWADRAT
            if (lista.get(zmienna).getClass() == Kwadrat.class) {
                Kwadrat k = (Kwadrat) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                punkt.x = k.getP().x - punkt.x;
                punkt.y = k.getP().y;
                k.setP(punkt);
                this.repaint();
            }
            ////////          TROJKAT
            if (lista.get(zmienna).getClass() == Trojkat.class) {
                Trojkat t = (Trojkat) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                Punkt punkt1 = new Punkt(20, 0);
                Punkt punkt2 = new Punkt(20, 0);
                //Ustawnienie punktu A
                punkt.x = t.getA().x - punkt.x;
                punkt.y = t.getA().y;
                t.setA(punkt);
                /////Ustawnienie punktu B
                punkt1.x = t.getB().x - punkt1.x;
                punkt1.y = t.getB().y;
                t.setB(punkt1);
                /////Ustawnienie punktu C
                punkt2.x = t.getC().x - punkt2.x;
                punkt2.y = t.getC().y;
                t.setC(punkt2);
                this.repaint();
            }
        }
        //////////////                                              PRZESUWANIE W GORE
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            System.out.println("strzalka w prawo");
            ////////////////// OKRAG
            if (lista.get(zmienna).getClass() == Okrag.class) {
                Okrag o = (Okrag) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                punkt.x = o.getP().x + punkt.x;
                punkt.y = o.getP().y;
                o.setP(punkt);
                this.repaint();
            }
            ////////////////// KWADRAT
            if (lista.get(zmienna).getClass() == Kwadrat.class) {
                Kwadrat k = (Kwadrat) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                punkt.x = k.getP().x + punkt.x;
                punkt.y = k.getP().y;
                k.setP(punkt);
                this.repaint();
            }
            ////////          TROJKAT
            if (lista.get(zmienna).getClass() == Trojkat.class) {
                Trojkat t = (Trojkat) lista.get(zmienna);
                Punkt punkt = new Punkt(20, 0);
                Punkt punkt1 = new Punkt(20, 0);
                Punkt punkt2 = new Punkt(20, 0);
                //Ustawnienie punktu A
                punkt.x = t.getA().x + punkt.x;
                punkt.y = t.getA().y;
                t.setA(punkt);
                /////Ustawnienie punktu B
                punkt1.x = t.getB().x + punkt1.x;
                punkt1.y = t.getB().y;
                t.setB(punkt1);
                /////Ustawnienie punktu C
                punkt2.x = t.getC().x + punkt2.x;
                punkt2.y = t.getC().y;
                t.setC(punkt2);
                this.repaint();
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent evt) {

    }
//////////////////////// ZAZNACZANIE PPM
    @Override
    public void mousePressed(MouseEvent evt) {
        if(evt.getButton()== MouseEvent.BUTTON3){
            System.out.println("nacisnales ppm");
            Punkt p0 = new Punkt(evt.getX(),evt.getY());
            System.out.println("Punkt x = "+ p0.getX()+" y = "+p0.getY());
            for(int i=0;i<lista.size();i++){
                if(lista.get(i).isInside(p0)) {
                    System.out.println("zaznaczyles figure");
                    zmienna = i;
                }
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent evt) {

    }

    @Override
    public void mouseEntered(MouseEvent evt) {

    }

    @Override
    public void mouseExited(MouseEvent evt) {

    }
}