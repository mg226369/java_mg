package com.company;
import java.awt.geom.*;
import java.awt.*;

public class Trojkat extends Figura {
    Punkt a;
    Punkt b;
    Punkt c;
    Trojkat(){
        a = new Punkt(10,10);
        b = new Punkt(100,10);
        c = new Punkt(10,100);
    }
    Trojkat(Punkt a,Punkt b,Punkt c){
        this.a=a;
        this.b=b;
        this.c=c;
    }
    public Shape getShape(){
        int xtrojkat[] = {this.a.getX(),this.b.getX(),this.c.getX()}; //tablice wspolrzednych x'owych i y'owych wierzcholkow trojkata
        int ytrojkat[] = {this.a.getY(),this.b.getY(),this.c.getY()};
        return new Polygon(xtrojkat,ytrojkat,3);
    }
    public boolean isInside(Punkt p0) {
        return getShape().contains(p0.getX(),p0.getY());
    }

    public Punkt getA() {
        return a;
    }

    public Punkt getB() {
        return b;
    }

    public Punkt getC() {
        return c;
    }

    public void setA(Punkt a) {
        this.a = a;
    }

    public void setB(Punkt b) {
        this.b = b;
    }

    public void setC(Punkt c) {
        this.c = c;
    }
}
