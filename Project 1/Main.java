package com.company;
import javax.swing.*;
import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		LinkedList<Figura> figury = new LinkedList<Figura>();
		Punkt p01 = new Punkt(150,150);
		Punkt p03 = new Punkt(400,350);
		Punkt p04 = new Punkt(600,350);
		Punkt p05 = new Punkt(800,350);
		Punkt p06 = new Punkt(600,650);
		Punkt p07 = new Punkt(1000,50);
		figury.add(new Okrag(250,p01));
		figury.add(new Kwadrat(p03,200));
		figury.add(new Trojkat(p04,p05,p06));
		figury.add(new Trojkat(p04,p07,p06));
		figury.add(new Kwadrat(p05,200));
		JFrame okno = new JFrame();
		Grafika graf = new Grafika(figury);
		okno.setSize(1100, 700);
		okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		okno.add(graf);
		okno.setVisible(true);

	}
}