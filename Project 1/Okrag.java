package com.company;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Okrag extends Figura {
    int promien;
    Punkt p;
    Okrag(){
        p = new Punkt();
        promien = 1;
    }
    Okrag(int promien, Punkt p){
        this.promien = promien;
        this.p=p;
    }

    public void setP(Punkt p) {
        this.p = p;
    }

    public void setPromien(int promien) {
        this.promien = promien;
    }

    public int getPromien() {
        return promien;
    }

    public Punkt getP() {
        return p;
    }
    public Shape getShape(){
        return new Ellipse2D.Double(this.p.getX(),this.p.getY(),this.promien,this.promien);
    }
    public boolean isInside(Punkt p0) {
        return getShape().contains(p0.getX(),p0.getY());
    }
}