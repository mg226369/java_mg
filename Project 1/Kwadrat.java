package com.company;
import java.awt.geom.*;
import java.awt.*;

public class Kwadrat extends Figura {
    Punkt p1;
    int dlugosc;
    Kwadrat(){
        p1 = new Punkt(0,0);
        dlugosc=100;
    }
    Kwadrat(Punkt p1,int dlugosc){
        this.p1=p1;
        this.dlugosc=dlugosc;
    }
    public Shape getShape(){
        return new Rectangle2D.Double(this.p1.getX(),this.p1.getY(),this.dlugosc,this.dlugosc);
    }
    public boolean isInside(Punkt p0) {
        return getShape().contains(p0.getX(),p0.getY());
    }
    public void setP(Punkt p) {
        this.p1 = p;
    }
    public Punkt getP() {
        return p1;
    }
}