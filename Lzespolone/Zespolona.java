import java.util.Random;

public class Zespolona {
    private int a;  // a - część rzeczywista
    private int b;  // b - część urojona
    Random r = new Random();

    public Zespolona(int a, int b) { //Wartości zadane
        this.a = a;
        this.b = b;
    }

    public Zespolona() { //wartości losowe
        this.a = r.nextInt(21) - 10;
        this.b = r.nextInt(21) - 10;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public double modul() { //moduł z liczby zespolonej
        return Math.sqrt((a * a) + (b * b));
    }

    public void wyswietlanie() {
        if ((b > 0 && a > 0) || (b > 0 && a < 0))
            System.out.println("x= " + this.a + "+" + this.b + "i");
        else if ((b < 0 && a > 0) || (b < 0 && a < 0))
            System.out.println("x= " + this.a + this.b + "i");
        else if (b == 0)
            System.out.println("x= " + this.a);
        else if (a == 0)
            System.out.println("x= " + this.b + "i");
        else
            System.out.println("x= 0");
    }

    public void sprzezenie() {
        int c = this.b * (-1);
        if ((c > 0 && a > 0) || (c > 0 && a < 0))
            System.out.println("x= " + this.a + "+" + c + "i");
        else if ((c < 0 && a > 0) || (c < 0 && a < 0))
            System.out.println("x= " + this.a + c + "i");
        else if (c == 0)
            System.out.println("x= " + this.a);
        else if (a == 0)
            System.out.println("x= " + c + "i");
        else
            System.out.println("x= 0");
    }
    public Zespolona dodawanie(Zespolona y){
        this.a = a + y.a;
        this.b = b +y.b;
        return this;
    }
    public Zespolona odejmowanie(Zespolona y) {
        this.a = a - y.a;
        this.b = b - y.b;
        return this;
    }
    public Zespolona mnozenie(Zespolona y) {
        int temp_a = this.a;
        int temp_b = this.b;
        this.a = temp_a*y.a + temp_b*y.b*(-1);
        this.b = temp_a*y.b + temp_b*y.a;
        return this;
    }
}